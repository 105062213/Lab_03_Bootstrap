# Software Studio 2018 Spring Lab03 Bootstrap and RWD
![Bootstrap logo](./Bootstrap logo.jpg)
## Grading Policy
* **Deadline: 2018/03/20 17:20 (commit time)**

## Todo
1. Check your username is Student ID
2. **Fork this repo to your account, remove fork relationship and change project visibility to public**
3. Make a personal webpage that has RWD
4. Use bootstrap from [CDN](https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css)
5. Use any template form the inernet but you should provide the URL in your project README.md
6. Use at least **Ten** bootstrap elements by yourself which is not include the template
7. You web page template should totally fit to your original personal web page content
8. Modify your markdown properly (check all todo and fill all item)
9. Deploy to your pages (https://[username].gitlab.io/Lab_03_Bootstrap)
10. Go home!

## Student ID , Name and Template URL
- [ ] Student ID : 105062213
- [ ] Name : 黃竣詳
- [ ] URL : https://www.w3schools.com/

## 10 Bootstrap elements (list in here)
1. Image Shapes(Thumbnail) https://www.w3schools.com/bootstrap4/bootstrap_images.asp
2. Buttons https://www.w3schools.com/bootstrap4/bootstrap_buttons.asp
3. Alerts https://www.w3schools.com/bootstrap4/bootstrap_alerts.asp
4. Colors https://www.w3schools.com/bootstrap4/bootstrap_colors.asp
5. Basic Table https://www.w3schools.com/bootstrap4/bootstrap_tables.asp
6. Collapse https://www.w3schools.com/bootstrap4/bootstrap_collapse.asp
7. Jumbotron https://www.w3schools.com/bootstrap4/bootstrap_jumbotron.asp
8. Badges https://www.w3schools.com/bootstrap4/bootstrap_badges.asp
9. Pagination https://www.w3schools.com/bootstrap4/bootstrap_pagination.asp
10. Cards https://www.w3schools.com/bootstrap4/bootstrap_cards.asp
